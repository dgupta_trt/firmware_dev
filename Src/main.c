/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

SDADC_HandleTypeDef hsdadc2;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t current_address[4] = {0x00,0x00,0x00,0x00}; // Mem Write Address
uint8_t start_address[4] = {0x00,0x00,0x00,0x00};
uint8_t read_address[4] = {0x00,0x00,0x00,0x00}; // Mem read address
const int* time_pointer = (int *)0x40002800; // points to time register of RTC
const int* time_subsecond = (int *)0x4002828; // points to subsecond register RTC
const int* time_date = (int *)0x4002804; // points to date register RTC
uint8_t RxBuffer[261]; // SPI Receive BUffer
uint8_t TxBuffer[261]; // SPI Transmit Buffer
uint8_t btTransmit[240]; // Data Transmit Read From Memory
uint8_t onebtPacket[20]; // Contains one bt Packet of data
uint8_t dozenbtPacket[240]; // Contains twelve bt Packets of data, for storage
uint8_t TimeStamp[5]; // current time
uint8_t EXT_ADC_CH0[15]; // Buffer Containing External ADC Values
volatile uint32_t adcVal; // contains read external ADC value
volatile int counter = 0; //counter within the interrupt callback function
int* PD8_Data_out = (int*)0x48000C10; // GPIO Register PD8, DOUT0
volatile int FLAG; //Set when one data point received from External ADC
int* PB2_DOUT0 = (int*)0x48000410; // DOUT0 GPIO PIN
uint32_t* EXTI_BASE_ADDR = (uint32_t*)0x40010400; // Interrupt Base Address
uint16_t HRBuffer[10]; //Contains ADC Output to be processed, HR alg
uint32_t TimeBuffer[10]; // Contains Times corresponding to ADC Output, HR alg
size_t downSample = 0; // down sampling counter, HR alg
uint32_t peakTime = 0; // Time at Peak, HR alg
uint32_t oldPeak = 0; // Time at previous peak, HR alg
short BPMint; // Calculated instantaneous BPM , HR alg
short avgBPM[10]; // BPM array to be averaged, HR alg

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SDADC2_Init(void);
static void MX_RTC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Erase_Memory_Chip(); // Erase Mem Chip
uint8_t Read_Status_Register(); // Read Mem Status Register
void Read_JEDEC_ID(); // Read Mem Manufacturer ID
uint8_t* Read_SDADC();// Read Data from Internal ADC
void Get_TimeStamp(); // Get Timestamp from RTC
void Get_BT_Data_Packet(uint8_t sdadc_vals[14]); //Fills oneBTPacket with SDADC (internal ADC) Data
void Increment_Current_Address(); //Increment Address Written to
void Increment_Read_Address(); //Increment Address Read at
_Bool Compare_Mem_Address(uint8_t addr1[4], uint8_t addr2[4]); // Compare the read and write address
void Store_256Bytes_Data(uint8_t address[4]); // Store 240 Bytes of Data to Memory
void Read_240Bytes_Data(uint8_t address[4]); // Read 240 Bytes of Data from Memory
void Get_12_BT_Packets(); //Accumulate 12 BT Packets of Data
void Set_4Byte_Address(); // Set the Mem to 4 Byte Addressing
void Mem_Write_Enable(); // Write Enable Bit set on Mem
uint8_t Read_Mem_SecReg(); // Read the Security Register on Mem
uint8_t Read_Mem_ConfigReg(); //Read the configuration Register on Mem
void Store_Random_Data(uint8_t address[4]); // Debugging Store Hard coded data to Mem
void Read_20Bytes_Data(uint8_t address[4]); // Debugging, Read 20 Bytes of Data (one BT Packet) from Mem
void Store_Byte(uint8_t address[4]); // Debugging Store one Byte of Data to Mem
void Read_Byte(uint8_t address[4]); // Debugging, Read one Byte of Data from Mem
short HeartRate(); // Returns calculated heart rate
size_t FindMaxIndex(); // Finds the index at the max of an array
void fillHRBuffer(); // Fills the HR Buffer and Time Buffer with input data
short meanBPM(); // Returns the mean of the calculated BPM values
void Read_EXTI_ADC_D0(); // Reads data from the External ADC, disabling and enabling the interrupts
void Get_BT_EXTADCData_Packet(); // Fills oneBTPacket with data from the External ADC

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  MX_SDADC2_Init();
  MX_RTC_Init();

  /* USER CODE BEGIN 2 */
  //Erase_Memory_Chip();
  HAL_Delay(7000); // Delay for setup
  Set_4Byte_Address(); //4 Byte Addressing on Mem
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	  // Check BT Connection Status, Working Implementation of BT and Mem Comm
	  _Bool BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
	  _Bool MEM_Check = Compare_Mem_Address(start_address, current_address);

	  // case of connected and at the starting memory location (don't need to divert to memory)
	  while(BT_Check && MEM_Check) {

		  // Internal SDADC (USE EITHER EXT OR INT)
		  Get_BT_Data_Packet(Read_SDADC());

		  // External ADC (USE EITHER EXT OR INT), To USE Un-comment next two lines, and comment out the
		  // line above

		  // Read_EXTI_ADC_D0();
		  // Get_BT_EXTADCData_Packet();

		  // Downsample data from ADC, Fill HR Buffer
		  downSample++;
		  if(downSample == 4){
			  fillHRBuffer();
			  downSample = 0;
		  }

		  HAL_UART_Transmit(&huart2, onebtPacket, 20, 100);
		  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
		  MEM_Check = Compare_Mem_Address(start_address, current_address);
	  }

	  // case of connected but not at starting memory location, read from memory and transmit, reset memory location
	  while( BT_Check && !MEM_Check){
		  // Write Page Takes at most 3 mS, so delay
		  HAL_Delay(5);
		  uint8_t StatusRegRead = Read_Status_Register();
		  if( (StatusRegRead & 0x01) == 0x00 ){ //Check Write NOT in progress
			  uint8_t StatusRegRead2 = Read_Status_Register();
			  if( StatusRegRead2 == 0x00 ){ // Check Block not Protected
				  Read_240Bytes_Data(read_address);
				  Increment_Read_Address();
				  HAL_UART_Transmit(&huart2, btTransmit, 240, 100);
			  }
		  }
		  else{ // If Write in progress, Wait
			  HAL_Delay(5);
		  }
		  // Check to See if read all data from Mem
			_Bool Read_Current = Compare_Mem_Address(read_address, current_address);
			if(Read_Current){
				for(size_t j = 0; j < 4; j++){
					current_address[j] = start_address[j];
				}
			}

			  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
			  MEM_Check = Compare_Mem_Address(start_address, current_address);
	  }

	  // case of not connected, store to memory starting at current address
	  while( !BT_Check){

		  // Downsample data from ADC, Fill HR Buffer
		  downSample++;
		  if(downSample == 4){
			  fillHRBuffer();
			  downSample = 0;
		  }

		  Mem_Write_Enable();
		  uint8_t StatusRegWEL = Read_Status_Register();
		  if( (StatusRegWEL & 0x02) == 0x02 )  { // Check Write Enable Bit
			  Store_256Bytes_Data(current_address); // To use External ADC modify this function
			  HAL_Delay(5); // Write at most takes 3 mS
			  uint8_t StatusRegWIP = Read_Status_Register();
			  if( (StatusRegWIP & 0x01) == 0x00 )  { // Check Write not in progress
				  uint8_t StatusRegBP = Read_Status_Register();
				  if( StatusRegBP == 0x00 )  { // Check not WEL and WIP
					  uint8_t SecReg = Read_Mem_SecReg();
					  if( (SecReg & 0x20) == 0x00){ // Check Write was successful
						  Increment_Current_Address();
					  }
				  }
			  }
			  else{ // Write in Progress, so Delay
				  HAL_Delay(10);
			  }
		  }
		  else{ // Write not enabled, wait and try again
			  HAL_Delay(10);
		  }

		  BT_Check = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_12) == GPIO_PIN_RESET;
		  MEM_Check = Compare_Mem_Address(start_address, current_address);
	  }


	// Debugging Tools for MEM (WORKS for ONE WRITE and THEN READ)
	// Commented out in normal operation, used to debug the memory write/read
	/*
	int k = 0;
	Mem_Write_Enable();
	uint8_t StatusReg = Read_Status_Register();
	if( (StatusReg & 0x02) == 0x02 )  {
	  Store_256Bytes_Data(current_address);
	  //Store_Random_Data(current_address);
	  //Store_Byte(current_address);
	  k = 1;
	  HAL_UART_Transmit(&huart2, TxBuffer, 245, 100);
	}
	else{
	  Mem_Write_Enable();
	  char WREN[5] = "WREN";
	  WREN[4] = StatusReg;
	  HAL_UART_Transmit(&huart2,(uint8_t*)WREN, 5, 100);
	}
	if (k == 1) {
	  HAL_Delay(5);
	  uint8_t StatusRegRead = Read_Status_Register();
	  if( (StatusRegRead & 0x01) == 0x00 ){
		  uint8_t StatusRegRead2 = Read_Status_Register();
		  if( StatusRegRead2 == 0x00 ){
			  Read_240Bytes_Data(current_address);
			  //HAL_UART_Transmit(&huart2, btTransmit, 240, 100);
			  //Read_20Bytes_Data(current_address);
			  //Read_Byte(current_address);
		  }
	  }
	  else{
		  char STAT[2];
		  STAT[0] = StatusRegRead;
		  HAL_Delay(5);
		  StatusRegRead = Read_Status_Register();
		  STAT[1] = Read_Status_Register();
		  HAL_UART_Transmit(&huart2,(uint8_t*)STAT, 2, 100);
		  Read_240Bytes_Data(current_address);
		  //HAL_UART_Transmit(&huart2, btTransmit, 240, 100);
		  //Read_Byte(current_address);
		  //Read_20Bytes_Data(current_address);
	  }
	HAL_Delay(3000);
	_Bool check = 1;
	size_t j = 0;
	while(j < 240 && check){
	  check = btTransmit[j] == TxBuffer[j+5];
	  j++;
	}
	if(check){
	 uint8_t true[1] = {0xAA};
	 HAL_UART_Transmit(&huart2, true ,1, 100);
	 HAL_UART_Transmit(&huart2, current_address ,4, 100);
	 Increment_Current_Address();
	}
	else{
	 uint8_t false[1] = {0xFF};
	 HAL_UART_Transmit(&huart2, false ,1, 100);
	 HAL_UART_Transmit(&huart2, current_address ,4, 100);
	 Increment_Current_Address();
	}

	HAL_Delay(3000);
	HAL_UART_Transmit(&huart2, btTransmit, 240, 100);
	}
	*/

  }

  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_SDADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.SdadcClockSelection = RCC_SDADCSYSCLK_DIV16;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG2);

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* RTC init function */
static void MX_RTC_Init(void)
{

  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initialize RTC and set the Time and Date 
    */
  sTime.Hours = 0;
  sTime.Minutes = 0;
  sTime.Seconds = 0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }

  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_APRIL;
  sDate.Date = 24;
  sDate.Year = 17;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SDADC2 init function */
static void MX_SDADC2_Init(void)
{

  SDADC_ConfParamTypeDef ConfParamStruct;

    /**Configure the SDADC low power mode, fast conversion mode,
    slow clock mode and SDADC1 reference voltage 
    */
  hsdadc2.Instance = SDADC2;
  hsdadc2.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
  hsdadc2.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
  hsdadc2.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
  hsdadc2.Init.ReferenceVoltage = SDADC_VREF_EXT;
  if (HAL_SDADC_Init(&hsdadc2) != HAL_OK)
  {
    Error_Handler();
  }

    /**Set parameters for SDADC configuration 0 Register 
    */
  ConfParamStruct.InputMode = SDADC_INPUT_MODE_SE_ZERO_REFERENCE;
  ConfParamStruct.Gain = SDADC_GAIN_1;
  ConfParamStruct.CommonMode = SDADC_COMMON_MODE_VSSA;
  ConfParamStruct.Offset = 0;
  if (HAL_SDADC_PrepareChannelConfig(&hsdadc2, SDADC_CONF_INDEX_0, &ConfParamStruct) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  huart2.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  huart2.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);

  /*Configure GPIO pins : PE2 PE3 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PD6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
  *EXTI_BASE_ADDR &= ~(1 << 5); // Mask DRDY, Disable Interrupt
  *(EXTI_BASE_ADDR) &= ~(1 << 6); // Mask DCLK
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

  if(GPIO_Pin == GPIO_PIN_5) // Check DRDY Interrupt
  {
	adcVal = 0;
	counter = 0;
	*(EXTI_BASE_ADDR) |= 1 << 6; //UnMask DCLK Interrupt
  }
  else
  {
	adcVal = (adcVal << 1) |  ( ((*PB2_DOUT0) >> 2) & 0x01); // Read PB2, DOUT0
	counter++; // Increment Counter

	if(counter == 24){
		*(EXTI_BASE_ADDR) &= ~(1 << 6); // Mask DCLK, When One Data point Collected
		FLAG = 1;
	}

  }
}

// Read One BT Packet worth of data from the External ADC put int EXT_ADC_CH0 Buffer
void Read_EXTI_ADC_D0(){
	*EXTI_BASE_ADDR |= 1 << 5; // UnMask DRDY, Enable Interrupt
	size_t i = 1;
	_Bool ReadFLAG = FLAG;
	while( i < 14){
		while(ReadFLAG != 1){
			ReadFLAG = FLAG; // Keep checking flag set by DCLK interrupt
		}
		*EXTI_BASE_ADDR &= ~(1 << 5); // Mask DRDY, Disable Interrupt
		EXT_ADC_CH0[0] = (adcVal >> 16) & 0x07; // Channel Info + Debug ADC
		EXT_ADC_CH0[i] = adcVal >> 8; // First Part of 16 bit Data
		EXT_ADC_CH0[i+1] = adcVal; // Second Part of 16 bit Data
		FLAG = 0;
		i = i + 2;
		if( i == 13 ){ // Read onBTPacket worth of Data completed
		}
		else{
			*EXTI_BASE_ADDR |= 1 << 5; // UnMask DRDY, Enable Interrupt
		}
	}
}
// Put Data from External ADC Data buffer into oneBTPacket for transmit or storage
void Get_BT_EXTADCData_Packet(){
	onebtPacket[0] = EXT_ADC_CH0[0]; // channel number + Debug ADC
	onebtPacket[0] = 0x01; // channel number
	short HR_Num = meanBPM();
	Get_TimeStamp();
	for( size_t i = 1; i < 20; i++){
		if(i < 4){
			onebtPacket[i] = TimeStamp[i-1];
		}
		else if (i == 4){
			onebtPacket[i] =  HR_Num >> 8 ; //heart rate first 8 bits
		}
		else if (i == 5 ){
			onebtPacket[i] = (uint8_t) HR_Num; // heart rate second 8 bits
		}
		else{
			onebtPacket[i] = EXT_ADC_CH0[i-6]; // ADC Vals into BT Packet
		}
	}
}
// Mem Status Register Read
uint8_t Read_Status_Register(){
	uint8_t StatusRegister[2] = {0x00,0x00};
	uint8_t RDSR[2] = {0x05,0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	HAL_SPI_TransmitReceive(&hspi2,RDSR,StatusRegister,2,HAL_GetTick()); // Read Status
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	return StatusRegister[1];
}
// Mem Manuf.ID
void Read_JEDEC_ID(){
	  uint8_t aTxBuffer[4] = {0x9F,0x00,0x00,0x00};
	  uint8_t databuffer[4];
	  HAL_SPIEx_FlushRxFifo(&hspi2);
	  HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	  HAL_SPI_TransmitReceive(&hspi2, aTxBuffer, databuffer, 4, 5000);
	  HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	  HAL_UART_Transmit(&huart2,databuffer, 4, 5000);

}
// Debugging Tool for Mem
void Store_Random_Data(uint8_t address[4]){
	 uint8_t Command_Address_Data[25];
	 Command_Address_Data[0]= 0x12;
	 // enter address to write to
	 for(size_t i = 1;i < 5; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 for(size_t j = 5; j < 25; j++ ){
		 Command_Address_Data[j] = 0x99;
	 }

	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	 HAL_SPI_Transmit(&hspi2,Command_Address_Data,25,HAL_GetTick());
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
}

// Debugging Tool for Mem
void Store_Byte(uint8_t address[4]){
	 uint8_t Command_Address_Data[6];
	 Command_Address_Data[0]= 0x12;
	 // enter address to write to
	 for(size_t i = 1;i < 5; i++ ){
		 Command_Address_Data[i] = address[i-1];
	 }
	 Command_Address_Data[5] = 0xDD;

	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	 HAL_SPI_Transmit(&hspi2,Command_Address_Data,6,HAL_GetTick());
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
}

// Debugging Tool for Mem
void Read_Byte(uint8_t address[4]){
	 uint8_t SendBuffer[6] = {0};
	 uint8_t ReadBuffer[6] = {0};
	 uint8_t TransmitBuffer[1] = {0};
	 SendBuffer[0] = 0x13; // normal read, fast read 0C
	 // enter address to read from
	 for(size_t i = 1;i < 5; i++ ){
		 SendBuffer[i] = address[i-1];
	 }
	 HAL_SPIEx_FlushRxFifo(&hspi2);
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low E4
	 HAL_SPI_TransmitReceive(&hspi2,SendBuffer,ReadBuffer,6,HAL_GetTick()); // Read Data
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6


	 TransmitBuffer[0] = ReadBuffer[5];
	 HAL_UART_Transmit(&huart2,TransmitBuffer, 1, 5000);
}

// Debugging Tool for Mem
void Read_20Bytes_Data(uint8_t address[4]){
	 uint8_t SendBuffer[25] = {0};
	 uint8_t ReadBuffer[25] = {0};
	 uint8_t TransmitBuffer[20] = {0};
	 SendBuffer[0] = 0x13; // normal read
	 // enter address to read from
	 for(size_t i = 1;i < 5; i++ ){
		 SendBuffer[i] = address[i-1];
	 }
	 HAL_SPIEx_FlushRxFifo(&hspi2);
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	 HAL_SPI_TransmitReceive(&hspi2,SendBuffer,ReadBuffer,25,HAL_GetTick()); // Read Data
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6

	 for(size_t j = 5; j < 25; j++) {
		 TransmitBuffer[j-5] = ReadBuffer[j];
	 }
	 HAL_UART_Transmit(&huart2,TransmitBuffer, 20, 5000);
}

// Read Data from the SDADC
uint8_t* Read_SDADC(){
	 const size_t n = 14;
	 HAL_SDADC_CalibrationStart(&hsdadc2,HAL_SDADC_STATE_CALIB);
	 HAL_SDADC_PollForCalibEvent(&hsdadc2,HAL_GetTick());
	 static uint8_t sdadc_out[14];
	 for(int i = 0; i < n-1; i= i + 2){
		 HAL_SDADC_Start(&hsdadc2);
		 HAL_SDADC_PollForConversion(&hsdadc2, HAL_GetTick());
		 uint16_t value = 0;
		 value = HAL_SDADC_GetValue(&hsdadc2);
		 HAL_SDADC_Stop(&hsdadc2);
		 sdadc_out[i+1] = value;
		 sdadc_out[i] = value >> 8;
	 }
	 return sdadc_out;
}
// Get Timestamp from the RTC
void Get_TimeStamp(){

	   uint16_t temp_ss = RTC->SSR;
	   TimeStamp[3] = temp_ss >> 8; // 8 bits of subseconds
	   TimeStamp[4] = (uint8_t) temp_ss; //last 8 bits of subseconds
	   (void) RTC->DR;
	   uint32_t tempTime = *time_pointer;
	   TimeStamp[0] = RTC_Bcd2ToByte((tempTime >> 16) & 0x7F); // hours + 24 hours/PM
	   TimeStamp[1] = RTC_Bcd2ToByte((tempTime >> 8)  & 0x7F); // reserved bit + 7 bits of minutes
	   TimeStamp[2] = RTC_Bcd2ToByte(tempTime & 0x7F); // reserved bit + 7 bits of seconds
}
// Put SDADC Values and put into BT Packet
void Get_BT_Data_Packet(uint8_t sdadc_vals[14]){
	onebtPacket[0] = 0x01; // channel number, hardcoded
	short HR_Num = meanBPM(); // calculated BPM average
	Get_TimeStamp();
	for( size_t i = 1; i < 20; i++){
		if(i < 4){
			onebtPacket[i] = TimeStamp[i-1];
		}
		else if (i == 4){
			onebtPacket[i] =  HR_Num >> 8 ; //heart rate first 8 bits
		}
		else if (i == 5 ){
			onebtPacket[i] = (uint8_t) HR_Num; // heart rate second 8 bits
		}
		else{
			onebtPacket[i] = sdadc_vals[i-6];
		}
	}
}

// Puts oneBTPacket into Buffer with 20 packets for storage
void Get_12_BT_Packets(){
	for(size_t i = 0; i < 12; i++){
		Get_BT_Data_Packet(Read_SDADC());
		// To Use External ADC, Uncomment line below, and comment line above
		// This line will fill onbtPacket with External ADC Data

		// Get_BT_EXTADCData_Packet();
		for(size_t j = 0; j < 20;j++){
			dozenbtPacket[i*20+j] = onebtPacket[j];
		}
	}
}

// Store 240 Bytes of Data to Mem
void Store_256Bytes_Data(uint8_t address[4]){
	 TxBuffer[0] = 0x12;
	 // enter address to write to
	 for(size_t i = 1;i < 5; i++ ){
		TxBuffer[i] = address[i-1];
	 }
	 // enter the data from the ADC (240 Bytes data points)
	 for(size_t j = 5; j < 245; j++ ){
		 TxBuffer[j] = dozenbtPacket[j-5];
	 }
	 // append zeroes to have complete 256 Byte transactions
	 for(size_t k = 245; k < 261; k++){
		 TxBuffer[k] = 0x00;
	 }

	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	 HAL_SPI_Transmit(&hspi2,TxBuffer,261,HAL_GetTick()); // Write Data
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
}

void Read_240Bytes_Data(uint8_t address[4]){
	 TxBuffer[0] = 0x13;
	 //TxBuffer[0] = 0x0C; // Fast Read

	 // enter address to read from
	 for(size_t i = 1;i < 5; i++ ){
		 TxBuffer[i] = address[i-1];
	 }
	 HAL_SPIEx_FlushRxFifo(&hspi2);
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	 HAL_SPI_TransmitReceive(&hspi2,TxBuffer,RxBuffer,245,HAL_GetTick()); // Read Data
	 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6

	 for(size_t j = 5; j < 245; j++) {
		 btTransmit[j-5] = RxBuffer[j];
	 }
}

void Erase_Memory_Chip(){
	 uint8_t Erase_Chip[1] = {0x60};
	 Mem_Write_Enable();
	 uint8_t StatusReg = Read_Status_Register();
	 if( (StatusReg & 0x02) == 0x02 )  {
		 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
		 HAL_SPI_Transmit(&hspi2,Erase_Chip,1,HAL_GetTick()); // Erase Chip
		 HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	 }
}

void Increment_Current_Address(){
	uint32_t current_address_int = (uint32_t) current_address[0] << 24 | (uint32_t) current_address[1] << 16 | (uint32_t) current_address[2] << 8 | (uint32_t) current_address[3];
	current_address_int = current_address_int + 0x100;
	current_address[0] = (current_address_int >> 24) & 0xFF;
	current_address[1] = (current_address_int >> 16) & 0xFF;
	current_address[2] = (current_address_int >> 8) & 0xFF;
	current_address[3] = current_address_int & 0xFF;
}

void Increment_Read_Address(){
	uint32_t read_address_int = (uint32_t) read_address[0] << 24 | (uint32_t) read_address[1] << 16 | (uint32_t) read_address[2] << 8 | (uint32_t) read_address[3];
	read_address_int = read_address_int + 0x100;
	read_address[0] = (read_address_int >> 24) & 0xFF;
	read_address[1] = (read_address_int >> 16) & 0xFF;
	read_address[2] = (read_address_int >> 8) & 0xFF;
	read_address[3] = read_address_int & 0xFF;
}

_Bool Compare_Mem_Address(uint8_t addr1[4], uint8_t addr2[4]){
	_Bool check = 1;
	size_t i = 0;
	while(check && i < 4){
		check = addr1[i] == addr2[i];
		i++;
	}
	return check;
}

void Set_4Byte_Address(){
	uint8_t Address_Size[1] = {0xB7};
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	HAL_SPI_Transmit(&hspi2,Address_Size,1,500); // Set Address Size to 4 bytes
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	HAL_Delay(500);
	uint8_t ConfReg = Read_Mem_ConfigReg();
	if( ( (ConfReg >> 5) & 0x01) == 0x01){
	}
	else{
		uint8_t Error[1] = {ConfReg};
		HAL_UART_Transmit(&huart2, Error, 1, 100); // Error Message Debug Tool
		Set_4Byte_Address();
	}
}

void Mem_Write_Enable(){
	uint8_t Write_Enable[1] = {0x06};
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
    HAL_SPI_Transmit(&hspi2,Write_Enable,1,500); // Write Enable
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
}

uint8_t Read_Mem_SecReg(){
	uint8_t RDSCUR[2] = {0x2B,0x00};
	uint8_t SecReg[2] = {0x00,0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	HAL_SPI_TransmitReceive(&hspi2,RDSCUR,SecReg,2,500); // Read Sec Reg
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	return SecReg[1];
}

uint8_t Read_Mem_ConfigReg(){
	uint8_t RDCR[2] = {0x15,0x00};
	uint8_t ConfReg[2] = {0x00,0x00};
	HAL_SPIEx_FlushRxFifo(&hspi2);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET); // CS Low D6
	HAL_SPI_TransmitReceive(&hspi2,RDCR,ConfReg,2,HAL_GetTick()); // Read Status
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET); // CS High D6
	return ConfReg[1];
}

// Takes ADC Data and puts it into HR Buffer and Time Buffer for calucations
void fillHRBuffer(){
	for(size_t i = sizeof(HRBuffer)/sizeof(HRBuffer[0]) -1 ; i > 0; i--) {
		HRBuffer[i] = HRBuffer[i-1];
		TimeBuffer[i] = TimeBuffer[i-1];
	}
	HRBuffer[0] = (uint16_t) (onebtPacket[6] << 8) | (uint16_t) onebtPacket[7];
	TimeBuffer[0] = ( (uint32_t) (TimeStamp[2] << 16) | (uint32_t) (TimeStamp[3] << 8) | (uint32_t) TimeStamp[4]) ;
}
// Peak Detection, Not Entirely Functional
short HeartRate(){
	size_t peak = FindMaxIndex();
	if(peak == (sizeof(HRBuffer)/sizeof(HRBuffer[0]))/2){
		oldPeak = peakTime;
		peakTime = TimeBuffer[peak];
		uint16_t newPeakSubSec = 1000 * ( hrtc.Init.SynchPrediv - (peakTime & 0x0000FFFF)) / (hrtc.Init.SynchPrediv + 1);
		uint16_t oldPeakSubSec = 1000 * (hrtc.Init.SynchPrediv - (oldPeak & 0x0000FFFF)) / (hrtc.Init.SynchPrediv + 1);
		uint32_t peakDiff = (peakTime >> 16) * 1000 + newPeakSubSec - ( (oldPeak >> 16) * 1000 + oldPeakSubSec);

		if(peakDiff < 1500 && peakDiff > 500){ // Threshold at 40 BPM
			float BPM = 1.0/(peakDiff) * 60000.0;
			int bpmInt = (int) BPM;
			BPMint = (short) bpmInt;
		}
		return BPMint;
	}
	else{
		return BPMint;

	}
}
// Takes previously calculated values of HR and averages them
short meanBPM(){
	int total = 0;
	for(size_t i = sizeof(avgBPM)/sizeof(avgBPM[0])-1; i > 0; i--){
		avgBPM[i] = avgBPM[i-1];
		total += avgBPM[i];
	}
	avgBPM[0] = HeartRate();
	total += avgBPM[0];
	return total/(sizeof(avgBPM)/sizeof(avgBPM[0]));
}

// Finds the index at the Max
size_t FindMaxIndex(){

	size_t k = 0;
	uint16_t max = HRBuffer[0];

	for (size_t i = 0; i < sizeof(HRBuffer)/sizeof(HRBuffer[0]); i++)
	{
	    if (HRBuffer[i] > max)
	    {
	        max = HRBuffer[i];
	        k = i;
	    }
	}
	return k;
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
